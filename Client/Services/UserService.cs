﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.Json;
using Client.Interfaces;
using Newtonsoft.Json;

namespace Client.Services;

public class UserService(ISocketWrapper _socketWrapper) : IUserService
    {
        private string currentRole;
        private string loggedInUser;
        private bool isLoggedIn;
        public bool IsLoggedIn => isLoggedIn;

        public string AddUser()
        {
            string usernamePrompt = ReceiveJsonData();
            Console.WriteLine(usernamePrompt);

            string username = Console.ReadLine();
            SendData(username);

            string passwordPrompt = ReceiveJsonData();
            Console.WriteLine(passwordPrompt);

            string password = Console.ReadLine();
            SendData(password);

            string result = ReceiveJsonData();
            return result;
        }
        
        public bool Login()
        {
            string usernamePrompt = ReceiveJsonData();
            Console.WriteLine(usernamePrompt);

            string username = Console.ReadLine();
            SendData(username);

            string passwordPrompt = ReceiveJsonData();
            Console.WriteLine(passwordPrompt);

            string password = Console.ReadLine();
            SendData(password);

            string response = ReceiveJsonData();

            if (response.Equals("loggedIn", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("\nLogin successful. Awaiting further commands.");
                isLoggedIn = true;
                loggedInUser = username;
                currentRole = "user"; 
                return true;
            }

            return false;
        }
        
        public string DeleteUser(string username)
        {
            if (!currentRole.Equals("admin", StringComparison.OrdinalIgnoreCase) && !username.Equals(GetLoggedInUser(), StringComparison.OrdinalIgnoreCase))
            {
                return "You do not have permission to delete this user.";
            }

            SendData($"delete {username}");
            string response = ReceiveJsonData();

            if (username.Equals(GetLoggedInUser(), StringComparison.OrdinalIgnoreCase) && response.Contains("has been deleted"))
            {
                Logout();
                Console.WriteLine("Your account has been deleted. You have been logged out.");
            }
            else
            {
                Console.WriteLine(response);
            }

            return response;
        }


        public string GetUserInfo(string username)
        {
            string query = "SELECT Username, Role FROM Users WHERE Username = @Username";

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                string dbUsername = reader.GetString(0);
                                string dbRole = reader.GetString(2);

                                return $"Username: {dbUsername}\nRole: {dbRole}";
                            }
                            else
                            {
                                return "User not found in the database.";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    return "An error occurred while retrieving the user information.";
                }
            }
        }


        public string GetCurrentRole()
        {
            return currentRole;
        }

        public string GetLoggedInUser()
        {
            return loggedInUser;
        }

        public void Logout()
        {
            isLoggedIn = false;
            loggedInUser = null;
            currentRole = null;
        }
        
        public void PrintUserInfo(string command)
        {
            SendData(command);

            string encodingString = ReceiveJsonData();

            if (encodingString.ToLower().Equals("approved"))
            {
                Console.WriteLine("\nEnter username you'd like to check");
                string username = Console.ReadLine();
                DefaultMessage(username);
            }
            else
            {
                var currentUser = GetLoggedInUser();
                DefaultMessage(currentUser);
            }
        }
        
        private void DefaultMessage(string command)
        {
            SendData(command);

            string jsonResponse = ReceiveJsonData();
            Console.WriteLine(jsonResponse);
        }
        
        private void SendData(string data)
        {
            string jsonData = JsonConvert.SerializeObject(new { command = data });
            byte[] messageSent = Encoding.ASCII.GetBytes(jsonData);
            _socketWrapper.Send(messageSent);
        }
        
        private string ReceiveJsonData()
        {
            byte[] bytes = new byte[1024];
            int numByte = _socketWrapper.Receive(bytes);
            string jsonString = Encoding.ASCII.GetString(bytes, 0, numByte);
            dynamic jsonResponse = JsonConvert.DeserializeObject(jsonString);
            return jsonResponse.command;
        }
        
        public string GetConnectionString()
        {
            string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string json = File.ReadAllText(Path.Combine(projectDir, "appsettings.json"));
        
            using (JsonDocument doc = JsonDocument.Parse(json))
            {
                JsonElement root = doc.RootElement;
                JsonElement connectionStrings = root.GetProperty("ConnectionStrings");
                return connectionStrings.GetProperty("FirstCSDbConnection").GetString();
            }    
        }
        
        private void LogError(Exception ex)
        {
            using (StreamWriter writer = new StreamWriter("error_log.txt", true))
            {
                writer.WriteLine($"{DateTime.Now}: {ex.Message}");
                writer.WriteLine($"{DateTime.Now}: {ex.StackTrace}");
            }
        }
    }