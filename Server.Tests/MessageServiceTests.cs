﻿using System;
using System.IO;
using System.Text;
using Moq;
using Newtonsoft.Json;
using Xunit;
using Server.Interfaces;
using Server.Services;

namespace Server.Tests
{
    public class MessageServiceTests
    {
        
        private readonly string _userFile;
        private readonly string _messageFile;
        private readonly Mock<ISocketWrapper> _mockSocket;
        private readonly MessageService _service;
        private readonly Mock<IUserService> _mockUserService;
        private readonly Mock<IMessageRepository> _mockMessageRepository;

        

        public MessageServiceTests()
        {
            _mockSocket = new Mock<ISocketWrapper>();
            _mockUserService = new Mock<IUserService>();
            _mockMessageRepository = new Mock<IMessageRepository>();
            _service = new MessageService(_mockUserService.Object, _mockMessageRepository.Object);  
            _service.SetClientSocket(_mockSocket.Object, _mockUserService.Object);
        }
        
        [Fact]
        public void SendMessage_ShouldSendCorrectMessage()
        {
            // Arrange
            var mockSocket = new Mock<ISocketWrapper>();
            var mockUserService = new Mock<IUserService>();
            var mockMessageRepository = new Mock<IMessageRepository>();

            var service = new MessageService(mockUserService.Object, mockMessageRepository.Object);
            service.SetClientSocket(mockSocket.Object, mockUserService.Object);

            byte[] capturedMessage = null;
            var receiveSequence = new Queue<string>(new[] { "testUser", "Test Message" });

            mockSocket.Setup(socket => socket.Send(It.IsAny<byte[]>()))
                .Callback<byte[]>(msg => capturedMessage = msg);

            mockSocket.Setup(socket => socket.Receive(It.IsAny<byte[]>()))
                .Returns((byte[] buffer) =>
                {
                    if (receiveSequence.Count == 0)
                        throw new InvalidOperationException("No more data to receive.");

                    var message = JsonConvert.SerializeObject(receiveSequence.Dequeue());
                    var messageBytes = Encoding.ASCII.GetBytes(message);
                    Array.Copy(messageBytes, buffer, messageBytes.Length);
                    return messageBytes.Length;
                });

            mockUserService.Setup(us => us.UserExists("testUser")).Returns(true);

            // Act
            service.SendMessage();

            // Assert
            string expectedMessage = "Message has been sent.";
            string jsonExpectedMessage = JsonConvert.SerializeObject(expectedMessage);
            byte[] expectedMessageBytes = Encoding.ASCII.GetBytes(jsonExpectedMessage);

            Assert.NotNull(capturedMessage);
            Assert.Equal(Encoding.ASCII.GetString(expectedMessageBytes), Encoding.ASCII.GetString(capturedMessage));
        }

        
        [Fact]
        public void SendMessage_ShouldReturnUserDoesNotExist_WhenUserDoesNotExist()
        {
            // Arrange
            var mockSocket = new Mock<ISocketWrapper>();
            var mockUserService = new Mock<IUserService>();
            var mockMessageRepository = new Mock<IMessageRepository>();
        
            var service = new MessageService(mockUserService.Object, mockMessageRepository.Object);
            service.SetClientSocket(mockSocket.Object, mockUserService.Object);
        
            byte[] capturedMessage = null;
        
            var receiveSequence = new Queue<string>(new[] { "nonexistentUser", "Test Message" });
        
            mockSocket.Setup(socket => socket.Send(It.IsAny<byte[]>()))
                .Callback<byte[]>(msg => capturedMessage = msg);
        
            mockSocket.Setup(socket => socket.Receive(It.IsAny<byte[]>()))
                .Returns((byte[] buffer) =>
                {
                    if (receiveSequence.Count == 0)
                        throw new InvalidOperationException("No more data to receive.");
        
                    var message = JsonConvert.SerializeObject(receiveSequence.Dequeue());
                    var messageBytes = Encoding.ASCII.GetBytes(message);
                    Array.Copy(messageBytes, buffer, messageBytes.Length);
                    return messageBytes.Length;
                });
        
            mockUserService.Setup(us => us.UserExists("nonexistentUser")).Returns(false);
        
            // Act
            service.SendMessage();
        
            // Assert
            string expectedMessage = "User doesn't exist.";
            string jsonExpectedMessage = JsonConvert.SerializeObject(expectedMessage);
            byte[] expectedMessageBytes = Encoding.ASCII.GetBytes(jsonExpectedMessage);
        
            Assert.NotNull(capturedMessage);
            Assert.Equal(Encoding.ASCII.GetString(expectedMessageBytes), Encoding.ASCII.GetString(capturedMessage));
        }

        [Fact]
        public void SendMessage_ShouldReturnMailboxFull_WhenMessageCountExceedsLimit()
        {
            // Arrange
            var mockSocket = new Mock<ISocketWrapper>();
            var mockUserService = new Mock<IUserService>();
            var mockMessageRepository = new Mock<IMessageRepository>();

            var service = new MessageService(mockUserService.Object, mockMessageRepository.Object);
            service.SetClientSocket(mockSocket.Object, mockUserService.Object);

            byte[] capturedMessage = null;

            var receiveSequence = new Queue<string>(new[] { "testUser123", "Test Message" });

            mockSocket.Setup(socket => socket.Send(It.IsAny<byte[]>()))
                .Callback<byte[]>(msg => capturedMessage = msg);

            mockSocket.Setup(socket => socket.Receive(It.IsAny<byte[]>()))
                .Returns((byte[] buffer) =>
                {
                    if (receiveSequence.Count == 0)
                        throw new InvalidOperationException("No more data to receive.");

                    var message = JsonConvert.SerializeObject(receiveSequence.Dequeue());
                    var messageBytes = Encoding.ASCII.GetBytes(message);
                    Array.Copy(messageBytes, buffer, messageBytes.Length);
                    return messageBytes.Length;
                });

            mockUserService.Setup(us => us.UserExists("testUser123")).Returns(true);

            mockMessageRepository.Setup(repo => repo.GetMessageCountForUser("testUser123")).Returns(5);

            // Act
            service.SendMessage();

            // Assert
            string expectedMessage = "Mailbox is full.";
            string jsonExpectedMessage = JsonConvert.SerializeObject(expectedMessage);
            byte[] expectedMessageBytes = Encoding.ASCII.GetBytes(jsonExpectedMessage);

            Assert.NotNull(capturedMessage);
            Assert.Equal(Encoding.ASCII.GetString(expectedMessageBytes), Encoding.ASCII.GetString(capturedMessage));
        }

        [Fact]
        public void ReadMessage_ShouldReturnMessage_WhenMessageIsAvailable()
        {
            // Arrange
            string expectedMessage = "Test Message";

            var mockSocket = new Mock<ISocketWrapper>();
            var mockUserService = new Mock<IUserService>();
            var mockMessageRepository = new Mock<IMessageRepository>();

            var service = new MessageService(mockUserService.Object, mockMessageRepository.Object);
            service.SetClientSocket(mockSocket.Object, mockUserService.Object);

            byte[] capturedMessage = null;

            mockSocket.Setup(socket => socket.Send(It.IsAny<byte[]>()))
                .Callback<byte[]>(msg => capturedMessage = msg);

            mockMessageRepository.Setup(repo => repo.GetFirstMessageForUser("testUser"))
                .Returns(expectedMessage);

            mockMessageRepository.Setup(repo => repo.DeleteMessage(expectedMessage));

            // Act
            service.ReadMessage("testUser");

            string jsonExpectedMessage = JsonConvert.SerializeObject(expectedMessage);
            byte[] expectedMessageBytes = Encoding.ASCII.GetBytes(jsonExpectedMessage);

            Assert.NotNull(capturedMessage);
            Assert.Equal(Encoding.ASCII.GetString(expectedMessageBytes), Encoding.ASCII.GetString(capturedMessage));

            mockMessageRepository.Verify(repo => repo.DeleteMessage(expectedMessage), Times.Once);
        }

        [Fact]
        public void ReadMessage_ShouldReturnNoNewMessages_WhenNoMessageIsAvailable()
        {
            // Arrange
            var mockSocket = new Mock<ISocketWrapper>();
            var mockUserService = new Mock<IUserService>();
            var mockMessageRepository = new Mock<IMessageRepository>();

            var service = new MessageService(mockUserService.Object, mockMessageRepository.Object);
            service.SetClientSocket(mockSocket.Object, mockUserService.Object);

            byte[] capturedMessage = null;

            mockSocket.Setup(socket => socket.Send(It.IsAny<byte[]>()))
                .Callback<byte[]>(msg => capturedMessage = msg);

            mockMessageRepository.Setup(repo => repo.GetFirstMessageForUser("testUser"))
                .Returns((string)null);

            // Act
            service.ReadMessage("testUser");

            string expectedMessage = "There are no new messages.";
            string jsonExpectedMessage = JsonConvert.SerializeObject(expectedMessage);
            byte[] expectedMessageBytes = Encoding.ASCII.GetBytes(jsonExpectedMessage);

            Assert.NotNull(capturedMessage);
            Assert.Equal(Encoding.ASCII.GetString(expectedMessageBytes), Encoding.ASCII.GetString(capturedMessage));

            mockMessageRepository.Verify(repo => repo.DeleteMessage(It.IsAny<string>()), Times.Never);
        }
        
        [Fact]
        public void ReadMessage_ShouldReturnNoNewMessages_WhenUserDoesNotExist()
        {
            // Arrange
            var mockSocket = new Mock<ISocketWrapper>();
            var mockUserService = new Mock<IUserService>();
            var mockMessageRepository = new Mock<IMessageRepository>();

            var service = new MessageService(mockUserService.Object, mockMessageRepository.Object);
            service.SetClientSocket(mockSocket.Object, mockUserService.Object);

            byte[] capturedMessage = null;

            mockSocket.Setup(socket => socket.Send(It.IsAny<byte[]>()))
                .Callback<byte[]>(msg => capturedMessage = msg);

            mockMessageRepository.Setup(repo => repo.GetFirstMessageForUser("nonexistentUser"))
                .Returns((string)null);

            // Act
            service.ReadMessage("nonexistentUser");

            
            string expectedMessage = "There are no new messages.";
            string jsonExpectedMessage = JsonConvert.SerializeObject(expectedMessage);
            byte[] expectedMessageBytes = Encoding.ASCII.GetBytes(jsonExpectedMessage);
            
            // Assert
            Assert.NotNull(capturedMessage);
            Assert.Equal(Encoding.ASCII.GetString(expectedMessageBytes), Encoding.ASCII.GetString(capturedMessage));

            mockMessageRepository.Verify(repo => repo.DeleteMessage(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void SendMessage_ShouldNotSendEmptyMessage()
        {
            // Arrange
            var mockSocket = new Mock<ISocketWrapper>();
            var mockUserService = new Mock<IUserService>();
            var mockMessageRepository = new Mock<IMessageRepository>();

            var service = new MessageService(mockUserService.Object, mockMessageRepository.Object);
            service.SetClientSocket(mockSocket.Object, mockUserService.Object);

            string capturedMessage = null;

            mockSocket.Setup(socket => socket.Send(It.IsAny<byte[]>()))
                .Callback<byte[]>(msg => capturedMessage = Encoding.ASCII.GetString(msg));

            mockUserService.Setup(us => us.UserExists("testUser"))
                .Returns(true);

            mockSocket.Setup(socket => socket.Receive(It.IsAny<byte[]>()))
                .Returns(0);

            // Act
            service.SendMessage();

            // Assert
            Assert.DoesNotContain("Message has been sent.", capturedMessage);

            mockMessageRepository.Verify(repo => repo.StoreMessageInDatabase(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }


    }
}
