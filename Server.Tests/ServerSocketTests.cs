using System.Data.SqlClient;
using Moq;
using Xunit;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Server.Interfaces;
using Server.Services;

namespace Server.Tests
{
    public class ServerSocketTests
    {
        private readonly UserService _userService;
        private readonly Mock<ISocketWrapper> _mockSocketWrapper;
        private readonly Mock<IUserService> _mockUserService;
        private readonly ServerSocket _serverSocket;
        private readonly IConnectionPoolService _connectionPoolService;

        public ServerSocketTests()
        {
            var mockConnectionPoolService = new Mock<IConnectionPoolService>();

            var mockConnection = new Mock<SqlConnection>();
            mockConnectionPoolService.Setup(pool => pool.TakeConnection()).Returns(mockConnection.Object);
            mockConnectionPoolService.Setup(pool => pool.ReturnConnection(It.IsAny<SqlConnection>()));
            
            _connectionPoolService = mockConnectionPoolService.Object;

            _userService = new UserService(_connectionPoolService);
            _mockSocketWrapper = new Mock<ISocketWrapper>();
            _mockUserService = new Mock<IUserService>();

            _serverSocket = new ServerSocket(
                _mockUserService.Object,
                new Mock<IMessageService>().Object,
                new Mock<IServerInfoService>().Object,
                new Mock<IMessageRepository>().Object,
                new Mock<IConnectionPoolService>().Object
            );

            typeof(ServerSocket).GetField("clientSocket", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
                .SetValue(_serverSocket, _mockSocketWrapper.Object);
        }
        [Fact]
        public void PrintUserInfo_ShouldSendApproved_WhenUserIsAdmin()
        {
            // Arrange
            var username = "adminUser";
            var userInfo = "User Info: adminUser";
            var command = "approved";

            _mockUserService.Setup(m => m.GetLoggedInUser()).Returns(username);
            _mockUserService.Setup(m => m.GetCurrentRole()).Returns("admin");
            _mockUserService.Setup(m => m.GetUserInfo(It.IsAny<string>())).Returns(userInfo);

            _mockSocketWrapper.Setup(m => m.Receive(It.IsAny<byte[]>()))
                .Callback<byte[]>(buffer =>
                {
                    var jsonString = JsonConvert.SerializeObject(new { command = username });
                    var bytes = Encoding.ASCII.GetBytes(jsonString);
                    bytes.CopyTo(buffer, 0);
                })
                .Returns((byte[] buffer) => Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(new { command = username })).Length);

            // Act
            _serverSocket.PrintUserInfo();

            // Assert
            _mockSocketWrapper.Verify(m => m.Send(It.Is<byte[]>(b => Encoding.ASCII.GetString(b).Contains(command))), Times.Once);
            _mockSocketWrapper.Verify(m => m.Send(It.Is<byte[]>(b => Encoding.ASCII.GetString(b).Contains(userInfo))), Times.Once);
        }
        [Fact]
        public void PrintUserInfo_ShouldSendNotApproved_WhenUserIsNotAdmin()
        {
            // Arrange
            var username = "regularUser";
            var userInfo = "User Info: regularUser";
            var command = "not approved";

            _mockUserService.Setup(m => m.GetLoggedInUser()).Returns(username);
            _mockUserService.Setup(m => m.GetCurrentRole()).Returns("user");
            _mockUserService.Setup(m => m.GetUserInfo(It.IsAny<string>())).Returns(userInfo);

            _mockSocketWrapper.Setup(m => m.Receive(It.IsAny<byte[]>()))
                .Callback<byte[]>(buffer =>
                {
                    var jsonString = JsonConvert.SerializeObject(new { command = username });
                    var bytes = Encoding.ASCII.GetBytes(jsonString);
                    bytes.CopyTo(buffer, 0);
                })
                .Returns((byte[] buffer) => Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(new { command = username })).Length);

            // Act
            _serverSocket.PrintUserInfo();

            // Assert
            _mockSocketWrapper.Verify(m => m.Send(It.Is<byte[]>(b => Encoding.ASCII.GetString(b).Contains(command))), Times.Once);
            _mockSocketWrapper.Verify(m => m.Send(It.Is<byte[]>(b => Encoding.ASCII.GetString(b).Contains(userInfo))), Times.Once);
        }
        
        [Fact]
        public void PrintUserInfo_ShouldReturnError_WhenUsernameIsNull()
        {
            // Arrange
            _mockUserService.Setup(m => m.GetLoggedInUser()).Returns((string)null);

            // Act
            _serverSocket.PrintUserInfo();

            // Assert
            _mockSocketWrapper.Verify(m => m.Send(It.Is<byte[]>(b => Encoding.ASCII.GetString(b).Contains("Username is required."))), Times.Once);
        }
        [Fact]
        public void PrintUserInfo_ShouldReturnError_WhenRoleIsInvalid()
        {
            // Arrange
            var username = "someUser";
            var invalidRole = "invalidRole";  

            _mockUserService.Setup(m => m.GetLoggedInUser()).Returns(username);
            _mockUserService.Setup(m => m.GetCurrentRole()).Returns(invalidRole);

            _mockSocketWrapper.Setup(m => m.Receive(It.IsAny<byte[]>()))
                .Returns(0); 

            // Act
            _serverSocket.PrintUserInfo();

            // Assert
            _mockSocketWrapper.Verify(m => m.Send(It.Is<byte[]>(b => Encoding.ASCII.GetString(b).Contains("Invalid command."))), Times.Once);
        }




        [Fact]
        public void DeleteUser_ShouldDeleteExistingUser_FromDatabase()
        {
            // Arrange
            var username = "usertodelete";
            var password = "password123";

            // Mock a SqlConnection for use in AddUser and DeleteUser
            var mockConnection = new Mock<SqlConnection>();
            _connectionPoolService.Setup(pool => pool.BorrowConnection()).Returns(mockConnection.Object);

            // Add the user (this simulates the behavior of adding a user to the database)
            _userService.AddUser(username, password);

            // Act - Delete the user
            var result = _userService.DeleteUser(username);

            // Assert
            Assert.Equal($"User {username} has been deleted.", result);
            Assert.False(_userService.UserExists(username));

            // Verify the connection was returned to the pool
            _connectionPoolService.Verify(pool => pool.ReturnConnection(It.IsAny<SqlConnection>()), Times.Once);
        }


        
        [Fact]
        public void DeleteUser_ShouldNotDeleteNonExistingUser()
        {
            // Arrange
            var username = "nonexistinguser";
            var expectedFilePath = $"{username}.json";

            // Act
            var result = _userService.DeleteUser(username);

            // Assert
            Assert.Equal($"User {username} does not exist.", result);
            Assert.False(File.Exists(expectedFilePath));
        }
        
    }
}