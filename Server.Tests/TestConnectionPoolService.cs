﻿using System.Data.SqlClient;
using Moq;
using Server.Services;

namespace Server.Tests;

public class TestConnectionPoolService : ConnectionPoolService
{
    public TestConnectionPoolService(string connectionString, int initialPoolSize = 10, int maxPoolSize = 100)
        : base(connectionString, initialPoolSize, maxPoolSize) { }

    public override SqlConnection TakeConnection()
    {
        return new Mock<SqlConnection>().Object;
    }

    public override void ReturnConnection(SqlConnection connection)
    {
    }
}
