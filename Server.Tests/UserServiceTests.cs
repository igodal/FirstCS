﻿using System.Data.SqlClient;
using System.IO;
using Moq;
using Newtonsoft.Json;
using Server.Interfaces;
using Server.Services;
using Xunit;

namespace Server.Tests;
public class UserServiceTests
{
    private readonly UserService _userService;
    private readonly IConnectionPoolService connectionPoolService;
    
    private readonly Mock<IConnectionPoolService> _mockConnectionPool;
    private readonly Mock<IDbConnectionWrapper> _mockConnection;

    public UserServiceTests()
    {
        _mockConnectionPool = new Mock<IConnectionPoolService>();
        _mockConnection = new Mock<IDbConnectionWrapper>();
        
        // Set up the mock connection pool to return the mock connection
        _mockConnectionPool.Setup(pool => pool.TakeConnection()).Returns(_mockConnection.Object);
        
        _userService = new UserService(_mockConnectionPool.Object);
        
        _userService = new UserService(connectionPoolService);
    }
    
    [Fact]
    public void AddUser_ShouldAddNewUser()
    {
        // Arrange
        var username = "123aaa";
        var password = "password123";

        // Just in case clean up
        DeleteUserFromDatabase(username);

        // Act
        var result = _userService.AddUser(username, password);

        // Assert
        Assert.Equal($"User {username} has been added.", result);
        Assert.True(_userService.UserExists(username));

        // Clean up
        DeleteUserFromDatabase(username);
    }

    [Fact]
    public void AddUser_ShouldNotAddExistingUser()
    {
        // Arrange
        var username = "existinguser";
        var password = "password123";
        
        _userService.AddUser(username, password);

        // Act
        var result = _userService.AddUser(username, password);

        // Assert
        Assert.Equal($"User {username} already exists.", result);

        // Clean up
        DeleteUserFromDatabase(username);
    }
    
    [Fact]
    public void AddUser_ShouldReturnError_WhenUsernameIsEmpty()
    {
        // Arrange
        var username = "";
        var password = "password123";

        // Act
        var result = _userService.AddUser(username, password);

        // Assert
        Assert.Equal("Username cannot be empty.", result);
    }
    [Fact]
    public void AddUser_ShouldReturnError_WhenPasswordIsEmpty()
    {
        // Arrange
        var username = "testuser";
        var password = "";

        // Act
        var result = _userService.AddUser(username, password);

        // Assert
        Assert.Equal("Password cannot be empty.", result);
    }
    
    [Fact]
    public void AddUser_ShouldNotAllowDuplicateUsernamesWithDifferentCasing()
    {
        // Arrange
        var username = "testuser";
        var password = "password123";
        var duplicateUsername = "TestUser"; // Different casing
    
        _userService.AddUser(username, password);

        // Act
        var result = _userService.AddUser(duplicateUsername, password);

        // Assert
        Assert.Equal($"User {duplicateUsername} already exists.", result);
        
        DeleteUserFromDatabase(username);
    }
    
    [Fact]
    public void AddUser_ShouldReturnError_WhenUsernameContainsInvalidCharacters()
    {
        // Arrange
        var username = "testuser'; DROP TABLE Users; --";
        var password = "password123";
    
        // Act
        var result = _userService.AddUser(username, password);
    
        // Assert
        Assert.Equal("Username contains invalid characters.", result);
    }

    [Fact]
    public void AddUser_ShouldReturnError_WhenMaximumUsersLimitIsReached()
    {
        // Arrange
        for (int i = 0; i < 100; i++)
        {
            _userService.AddUser($"user{i}", "password123");
        }

        // Act
        var result = _userService.AddUser("newuser", "password123");

        // Assert
        Assert.Equal("Maximum user limit reached.", result);

        // Clean up
        for (int i = 0; i < 100; i++)
        {
            DeleteUserFromDatabase($"user{i}");
        }
    }

    

    [Fact]
    public void Login_ShouldReturnTrueAndLoggedIn_WhenCredentialsAreCorrect()
    {
        // Arrange
        var username = "testuser";
        var password = "password123";

        _userService.AddUser(username, password);

        // Act
        var result = _userService.Login(username, password);

        // Assert
        Assert.True(result.Item1);
        Assert.Equal("loggedIn", result.Item2);
        Assert.True(_userService.IsLoggedIn());
        Assert.Equal(username, _userService.GetLoggedInUser());
        Assert.Equal("user", _userService.GetCurrentRole());

        // Clean up
        DeleteUserFromDatabase(username);
    }

    [Fact]
    public void Login_ShouldReturnFalseAndIncorrectPassword_WhenPasswordIsIncorrect()
    {
        // Arrange
        var username = "testuser";
        var password = "password123";
        var incorrectPassword = "wrongpassword";

        _userService.AddUser(username, password);

        // Act
        var result = _userService.Login(username, incorrectPassword);

        // Assert
        Assert.False(result.Item1);
        Assert.Equal("Incorrect password!", result.Item2);
        Assert.False(_userService.IsLoggedIn());
        Assert.Null(_userService.GetLoggedInUser());

        // Clean up
        DeleteUserFromDatabase(username);
    }

    [Fact]
    public void Login_ShouldReturnFalseAndUserDoesNotExist_WhenUserDoesNotExist()
    {
        // Arrange
        var username = "nonexistentuser";
        var password = "password123";

        // Act
        var result = _userService.Login(username, password);

        // Assert
        Assert.False(result.Item1);
        Assert.Equal("User doesn't exist.", result.Item2);
        Assert.False(_userService.IsLoggedIn());
        Assert.Null(_userService.GetLoggedInUser());
    }
    
    [Fact]
    public void GetUserInfo_ShouldReturnCorrectUserInfo_WhenUserExists()
    {
        // Arrange
        var username = "testuser";
        var password = "password123";
        var role = "user";
        var expectedUserInfo = $"Username: {username}\nPassword: {password}\nRole: {role}";
        
        var user = new User
        {
            Userame = username,
            Password = password,
            Role = role
        };
        File.WriteAllText($"{username}.json", JsonConvert.SerializeObject(user));

        // Act
        var result = _userService.GetUserInfo(username);

        // Assert
        Assert.Equal(expectedUserInfo, result);
        
        File.Delete($"{username}.json");
    }

    [Fact]
    public void GetUserInfo_ShouldReturnErrorMessage_WhenUserDoesNotExist()
    {
        // Arrange
        var username = "nonexistentuser";
        var expectedMessage = "User file not found.";

        // Act
        var result = _userService.GetUserInfo(username);

        // Assert
        Assert.Equal(expectedMessage, result);
    }
    
    [Fact]
    public void DeleteUser_ShouldDeleteUser_WhenUsernameIsValid()
    {
        // Arrange
        var username = "testuser";

        // Set up the mock command to simulate a successful delete operation
        var mockCommand = new Mock<SqlCommand>();
        mockCommand.Setup(cmd => cmd.ExecuteNonQuery()).Returns(1);
        _mockConnection.Setup(conn => conn.CreateCommand()).Returns(mockCommand.Object);

        // Act
        var result = _userService.DeleteUser(username);

        // Assert
        Assert.Equal($"User {username} has been deleted.", result);

        // Verify that TakeConnection and ReturnConnection are called
        _mockConnectionPool.Verify(pool => pool.TakeConnection(), Times.Once);
        _mockConnectionPool.Verify(pool => pool.ReturnConnection(It.IsAny<IDbConnectionWrapper>()), Times.Once);
        
        // Verify that ExecuteNonQuery is called once on the command
        mockCommand.Verify(cmd => cmd.ExecuteNonQuery(), Times.Once);
    }
    
    private void DeleteUserFromDatabase(string username)
    {
        if (string.IsNullOrWhiteSpace(username))
            return;

        var connection = connectionPoolService.TakeConnection();
        try
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM Users WHERE Username = @Username";
                command.Parameters.AddWithValue("@Username", username);
                command.ExecuteNonQuery();
            }
        }
        finally
        {
            connectionPoolService.ReturnConnection(connection);
        }
    }

    [Fact]
    public void DeleteUser_ShouldReturnError_WhenUsernameIsNull()
    {
        // Arrange
        string username = null;

        // Act
        var result = _userService.DeleteUser(username);

        // Assert
        Assert.Equal("Username cannot be null or empty.", result);
    }

}
