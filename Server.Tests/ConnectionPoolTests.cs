﻿using Moq;
using Server.Interfaces;
using Server.Services;

namespace Server.Tests;

public class ConnectionPoolTests
    {
        private readonly string _connectionString = "Server=.;Database=FirstCSDb;Integrated Security=True;";

        [Fact]
        public void TakeConnection_ShouldReturnAvailableConnection()
        {
            // Arrange
            var mockConnection = new Mock<IDbConnectionWrapper>();
            var connectionPool = new ConnectionPoolService(_connectionString, 5);
            var mockPool = new Mock<IConnectionPoolService>();
            mockPool.Setup(pool => pool.TakeConnection()).Returns(mockConnection.Object);

            // Act
            var connection = mockPool.Object.TakeConnection();

            // Assert
            Assert.NotNull(connection);
            connectionPool.ReturnConnection(connection);
        }

        [Fact]
        public void TakeConnection_WhenPoolIsEmpty_ShouldCreateNewConnection()
        {
            // Arrange
            var mockConnection1 = new Mock<IDbConnectionWrapper>();
            var mockConnection2 = new Mock<IDbConnectionWrapper>();
            var mockPool = new Mock<IConnectionPoolService>();
            mockPool.SetupSequence(pool => pool.TakeConnection())
                .Returns(mockConnection1.Object)
                .Returns(mockConnection2.Object);

            var firstConnection = mockPool.Object.TakeConnection();

            // Act
            var secondConnection = mockPool.Object.TakeConnection();

            // Assert
            Assert.NotNull(secondConnection);
            Assert.NotEqual(firstConnection, secondConnection);
        }

        [Fact]
        public void TakeConnection_WhenMaxPoolSizeReached_ShouldThrowException()
        {
            // Arrange
            var connectionPool = new ConnectionPoolService(_connectionString, poolSize: 1, maxPoolSize: 1);
            var firstConnection = connectionPool.TakeConnection();

            // Act & Assert
            Assert.Throws<InvalidOperationException>(() => connectionPool.TakeConnection());

            connectionPool.ReturnConnection(firstConnection);
        }


        [Fact]
        public void ReturnConnection_ShouldAddConnectionBackToAvailableList()
        {
            // Arrange
            var mockConnection = new Mock<IDbConnectionWrapper>();
            var connectionPool = new ConnectionPoolService(_connectionString, 1);
            var mockPool = new Mock<IConnectionPoolService>();
            mockPool.Setup(pool => pool.TakeConnection()).Returns(mockConnection.Object);

            var connection = mockPool.Object.TakeConnection();

            // Act
            connectionPool.ReturnConnection(connection);
            var newConnection = mockPool.Object.TakeConnection();

            // Assert
            Assert.Equal(connection, newConnection);
            connectionPool.ReturnConnection(newConnection);
        }

        [Fact]
        public void ReturnConnection_ShouldDisposeConnection_WhenExceedingPoolSize()
        {
            // Arrange
            var connectionPool = new ConnectionPoolService(_connectionString, poolSize: 1, maxPoolSize: 1);
            var firstConnection = connectionPool.TakeConnection();

            // Act
            Assert.Throws<InvalidOperationException>(() => connectionPool.TakeConnection());

            connectionPool.ReturnConnection(firstConnection);
            var secondConnection = connectionPool.TakeConnection();

            // Assert
            Assert.NotNull(secondConnection);
            connectionPool.ReturnConnection(secondConnection);
        }
    }