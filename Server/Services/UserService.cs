﻿using System;
using System.IO;
using Newtonsoft.Json;
using Server.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace Server.Services;
public class UserService(IConnectionPoolService connectionPoolService) : IUserService
{
    private string currentRole;
    private string loggedInUser;
    private bool isLoggedIn;
    public string AddUser(string username, string password)
    {
        
        string connectionString = GetConnectionString();
        int maxUsersLimit = 100;
        
        if (GetUserCount() >= maxUsersLimit)
        {
            return "Maximum user limit reached.";
        }
        
        if (string.IsNullOrWhiteSpace(username))
        {
            return "Username cannot be empty.";
        }
        if (ContainsInvalidCharacters(username))
        {
            return "Username contains invalid characters.";
        }
        if (UserExists(username))
        {
            return $"User {username} already exists.";
        }

        if (string.IsNullOrWhiteSpace(password))
        {
            return "Password cannot be empty.";
        }
        
        

        var (hashedPassword, salt) = HashPasswordPBKDF2(password);

        string query = "INSERT INTO Users (Username, Password, Salt, Role) VALUES (@Username, @Password, @Salt, @Role)";

        try
        {
            var connection = connectionPoolService.TakeConnection();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;
                command.Parameters.AddWithValue("@Username", username);
                command.Parameters.AddWithValue("@Password", hashedPassword);
                command.Parameters.AddWithValue("@Salt", salt);
                command.Parameters.AddWithValue("@Role", "user");

                int result = command.ExecuteNonQuery();

                connectionPoolService.ReturnConnection(connection);
                if (result > 0)
                {
                    return $"User {username} has been added.";
                }
                else
                {
                    return "Failed to add the user.";
                }
            }

        }
        catch (Exception ex)
        {
            LogError(ex);
            return "An error occurred while processing your request. Please try again later.";
        }
    }

    public string GetConnectionString()
    {
        string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
        string json = File.ReadAllText(Path.Combine(projectDir, "appsettings.json"));
        
        using (JsonDocument doc = JsonDocument.Parse(json))
        {
            JsonElement root = doc.RootElement;
            JsonElement connectionStrings = root.GetProperty("ConnectionStrings");
            return connectionStrings.GetProperty("FirstCSDbConnection").GetString();
        }    
    }

    public (bool, string) Login(string username, string password)
    {
        string query = "SELECT Password, Salt, Role FROM Users WHERE Username = @Username";

        try
        {
            var connection = connectionPoolService.TakeConnection();

            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;
                command.Parameters.AddWithValue("@Username", username);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string storedHash = reader.GetString(0);
                        string storedSalt = reader.GetString(1);
                        string role = reader.GetString(2);

                        string inputHash = HashPasswordSHA256(password);
                        connectionPoolService.ReturnConnection(connection);

                        if (VerifyPasswordPBKDF2(password, storedHash, storedSalt))
                        {
                            loggedInUser = username;
                            isLoggedIn = true;
                            currentRole = role;
                            return (true, "loggedIn");
                        }
                        else
                        {
                            return (false, "Incorrect password!");
                        }
                    }
                    else
                    {
                        return (false, "User doesn't exist.");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogError(ex);

            return (false, "An error occurred while processing your request. Please try again later.");
        }
    }
    private string HashPasswordSHA256(string password)
    {
        using (SHA256 sha256 = SHA256.Create())
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] hashBytes = sha256.ComputeHash(passwordBytes);
            return Convert.ToBase64String(hashBytes);
        }
    }
    
    private (string hashedPassword, string salt) HashPasswordPBKDF2(string password)
    {
        byte[] saltBytes = new byte[16];
        using (var rng = new RNGCryptoServiceProvider())
        {
            rng.GetBytes(saltBytes);
        }
        string salt = Convert.ToBase64String(saltBytes);

        using (var pbkdf2 = new Rfc2898DeriveBytes(password, saltBytes, 10000))
        {
            byte[] hashBytes = pbkdf2.GetBytes(20); // 160-bit hash
            return (Convert.ToBase64String(hashBytes), salt);
        }
    }
    
    private bool VerifyPasswordPBKDF2(string password, string storedHash, string storedSalt)
    {
        byte[] saltBytes = Convert.FromBase64String(storedSalt);

        using (var pbkdf2 = new Rfc2898DeriveBytes(password, saltBytes, 10000))
        {
            byte[] hashBytes = pbkdf2.GetBytes(20); // 160-bit hash

            return Convert.ToBase64String(hashBytes) == storedHash;
        }
    }
    
    public string DeleteUser(string username)
    {
        if (string.IsNullOrWhiteSpace(username))
        {
            return "Username cannot be null or empty.";
        }

        bool isSelfDeletion = username.Equals(loggedInUser, StringComparison.OrdinalIgnoreCase);
        if (!currentRole.Equals("admin", StringComparison.OrdinalIgnoreCase) && !isSelfDeletion)
        {
            return "You do not have permission to delete this user.";
        }

        string deleteQuery = "UPDATE Users SET IsDeleted = 1 WHERE Username = @Username";

        try
        {
            var connection = connectionPoolService.TakeConnection();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = deleteQuery;
                command.Parameters.AddWithValue("@Username", username);

                int result = command.ExecuteNonQuery();
                connectionPoolService.ReturnConnection(connection);

                if (result > 0)
                {
                    if (isSelfDeletion)
                    {
                        Logout();
                        return $"Your account ({username}) has been deleted and you have been logged out.";
                    }
                    else
                    {
                        return $"User {username} has been deleted.";
                    }
                }
                else
                {
                    return "User not found or failed to delete.";
                }
            }
        }
        catch (Exception ex)
        {
            LogError(ex);
            return "An error occurred while deleting the user.";
        }
    }

    
    public string GetUserInfo(string username)
    {
        string query = "SELECT Username, Role FROM Users WHERE Username = @Username";

        try
        {
            var connection = connectionPoolService.TakeConnection();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;
                command.Parameters.AddWithValue("@Username", username);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string dbUsername = reader.GetString(0);
                        string dbRole = reader.GetString(1);
                        connectionPoolService.ReturnConnection(connection);

                        return $"Username: {dbUsername}\nRole: {dbRole}";
                    }
                    else
                    {
                        return "User not found in the database.";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogError(ex);
            return "An error occurred while retrieving the user information.";
        }
    }

    public string GetCurrentRole()
    {
        return currentRole;
    }

    public string GetLoggedInUser()
    {
        return loggedInUser;
    }
    public void Logout()
    {
        isLoggedIn = false;
        loggedInUser = null;
        currentRole = null;
    }
    public bool IsLoggedIn()
    {
        return isLoggedIn;
    }
    private void LogError(Exception ex)
    {
        using (StreamWriter writer = new StreamWriter("error_log.txt", true))
        {
            writer.WriteLine($"{DateTime.Now}: {ex.Message}");
            writer.WriteLine($"{DateTime.Now}: {ex.StackTrace}");
        }
    }
    
    public bool UserExists(string username)
    {
        string query = "SELECT COUNT(*) FROM Users WHERE Username = @Username";

        var connection = connectionPoolService.TakeConnection();

        using (var command = connection.CreateCommand())
        {
            command.CommandText = query;
            command.Parameters.AddWithValue("@Username", username);

            int count = (int)command.ExecuteScalar();
            
            connectionPoolService.ReturnConnection(connection);

            return count > 0;
        }
    }
    private bool ContainsInvalidCharacters(string username)
    {
        var invalidCharacters = new[] { ':', ';', '\'', '-', '\"', '\\', '/' };

        foreach (var invalidChar in invalidCharacters)
        {
            if (username.Contains(invalidChar))
            {
                return true;
            }
        }

        return false;
    }
    private int GetUserCount()
    {
        string query = "SELECT COUNT(*) FROM Users";

        try
        {
            var connection = connectionPoolService.TakeConnection();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;
                var userCount = (int)command.ExecuteScalar();
                connectionPoolService.ReturnConnection(connection);

                return userCount;
            }
        }
        catch (Exception ex)
        {
            LogError(ex);
            return -1;
        }
    }

}