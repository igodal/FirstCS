﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using Newtonsoft.Json;
using Server.Interfaces;
using Server.Repositories;
using Microsoft.Extensions.Configuration;


namespace Server.Services
{
    public class ServerSocket
    {
        private ISocketWrapper clientSocket;
        private readonly static string serverVersion = "0.0.3";
        private readonly static DateTime serverCreationDate = DateTime.Now;
        private bool stopped = false;
        
        private readonly IUserService userService;
        private readonly IMessageService messageService;
        private readonly IServerInfoService serverInfoService;
        private readonly IMessageRepository messageRepository;
        private readonly IConfiguration _configuration;
        private readonly IConnectionPoolService _connectionPoolService;
        
        public ServerSocket(IUserService userService, 
            IMessageService messageService, 
            IServerInfoService serverInfoService,
            IMessageRepository messageRepository,
            IConnectionPoolService connectionPoolService)
        {
            this.userService = userService;
            this.messageService = messageService;
            this.serverInfoService = serverInfoService;
            this.messageRepository = messageRepository;
            _connectionPoolService = connectionPoolService;
        }
        
        static void Main(string[] args)
        {
            var connectionString = GetConnectionString();
            var connectionPoolService = new ConnectionPoolService(connectionString);
            var userService = new UserService(connectionPoolService);
            var messageRepository = new MessageRepository(connectionPoolService);
            var messageService = new MessageService(userService, messageRepository);
            var serverInfoService = new ServerInfoService(serverVersion, serverCreationDate);
            var serverSocket = new ServerSocket(userService, messageService, serverInfoService, messageRepository, connectionPoolService);
            serverSocket.StartServer();
        }

        public void StartServer()
        {
            IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHost.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11111);
            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                Console.WriteLine("Awaiting connection...");
                Socket acceptedSocket = listener.Accept();
                clientSocket = new SocketWrapper(acceptedSocket);
                messageService.SetClientSocket(clientSocket, userService);
                serverInfoService.SetClientSocket(clientSocket);
                
                var connectionString = _configuration.GetConnectionString("FirstCSDbConnection");

                Console.WriteLine("Connected");

                while (!stopped)
                {
                    string firstData = ReceiveData();
                    if (firstData == null)
                        break;

                    HandleFirstCommand(firstData.ToLower());

                    while (userService.IsLoggedIn())
                    {
                        HandleLoggedInCommands();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                listener.Close();
            }
        }
        
        private void HandleFirstCommand(string command)
        {
            switch (command)
            {
                case "login":
                    Login();
                    break;
                case "add":
                    AddUser();
                    break;
                default:
                    break;
            }
        }

        private void HandleLoggedInCommands()
        {
            if (stopped) return;
            SendData("Enter command (type \"help\" to check available commands):");
            string data = ReceiveData()?.ToLower();
            if (data == null || stopped)
                return;

            var commandActions = new Dictionary<string, Action>
            {
                { "add", AddUser },
                { "user", PrintUserInfo },
                { "help", serverInfoService.HelpCommand },
                { "info", serverInfoService.InfoCommand },
                { "uptime", serverInfoService.UptimeCommand },
                { "stop", StopCommand },
                { "logout", Logout },
                { "delete", DeleteUser },
                { "msg", () => messageService.SendMessage() },
                { "read", () => messageService.ReadMessage(userService.GetLoggedInUser()) }
            };

            if (commandActions.ContainsKey(data))
            {
                commandActions[data].Invoke();
            }
            else
            {
                IncorrectCommand();
            }
        }
        private void Login()
        {
            SendData("Enter username:");
            string username = ReceiveData();
            SendData("Enter password:");
            string password = ReceiveData();

            var (success, command) = userService.Login(username, password);
            if (success)
            {
                SendData(command);
            }
            else
            {
                SendData(command);
            }
        }
        public void SendData(string command)
        {
            if (stopped) return;
            try
            {
                string jsonData = JsonConvert.SerializeObject(new { command = command });
                byte[] messageSent = Encoding.ASCII.GetBytes(jsonData);
                clientSocket.Send(messageSent);
            }
            catch (ObjectDisposedException)
            {
                Console.WriteLine("Attempted to send data on a closed socket.");

            }
        }

        public string ReceiveData()
        {
            byte[] bytes = new byte[1024];
            int numByte = clientSocket.Receive(bytes);
            string jsonString = Encoding.ASCII.GetString(bytes, 0, numByte);
            dynamic jsonResponse = JsonConvert.DeserializeObject(jsonString);
            return jsonResponse.command;
        }


        public void PrintUserInfo()
        {
            string loggedInUser = userService.GetLoggedInUser();

            if (string.IsNullOrWhiteSpace(loggedInUser))
            {
                SendData("Username is required.");
                return;
            }

            var role = userService.GetCurrentRole();
            if (string.IsNullOrWhiteSpace(role))
            {
                SendData("User role is missing.");
                return;
            }
            string roleCommand = role.ToLower();
            string requestedUser;

            if (roleCommand.Equals("admin"))
            {
                SendData("approved");
            }
            else if (roleCommand.Equals("user"))
            {
                SendData("not approved");
            }
            else
            {
                SendData("Invalid command.");
                return;
            }

            requestedUser = ReceiveData();
            if (string.IsNullOrWhiteSpace(requestedUser))
            {
                SendData("Requested username cannot be empty.");
                return;
            }

            string userInfo = userService.GetUserInfo(requestedUser);
            SendData(userInfo);
        }


        private void Logout()
        {
            userService.Logout();
            SendData("You have been logged out.");
        }
        private void AddUser()
        {
            SendData("Enter username:");
            string username = ReceiveData();
            SendData("Enter password:");
            string password = ReceiveData();

            string result = userService.AddUser(username, password);
            SendData(result);
        }
        
        private void DeleteUser()
        {
            SendData("Enter username to delete:");
            string username = ReceiveData();

            string result = userService.DeleteUser(username);
            SendData(result);
        }


        private void IncorrectCommand()
        {
            SendData("Incorrect command. Type 'help' to get a list of available commands.");
        }

        private void StopCommand()
        {
            SendData("Server stopping...");

            stopped = true;

            clientSocket.Close();
            Console.WriteLine("Server socket closed, exiting application.");
        }
        public static string GetConnectionString()
        {
            string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string json = File.ReadAllText(Path.Combine(projectDir, "appsettings.json"));
        
            using (JsonDocument doc = JsonDocument.Parse(json))
            {
                JsonElement root = doc.RootElement;
                JsonElement connectionStrings = root.GetProperty("ConnectionStrings");
                return connectionStrings.GetProperty("FirstCSDbConnection").GetString();
            }    
        }
    }
}