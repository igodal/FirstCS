﻿using System.Data.SqlClient;
using Server.Interfaces;

namespace Server.Services;

public class DbConnectionWrapper : IDbConnectionWrapper
{
    private readonly SqlConnection _connection;

    public DbConnectionWrapper(string connectionString)
    {
        _connection = new SqlConnection(connectionString);
    }

    public void Open() => _connection.Open();
    public void Close() => _connection.Close();
    public SqlCommand CreateCommand() => _connection.CreateCommand();

    public void Dispose() => _connection.Dispose();
}
