﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using Newtonsoft.Json;
using Server.Interfaces;

namespace Server.Services
{
    public class MessageService : IMessageService
    {
        private ISocketWrapper _clientSocket;
        private IUserService _userService;
        private IMessageRepository _messageRepository;


        public MessageService(IUserService userService, IMessageRepository messageRepository)
        {
            _userService = userService;
            _messageRepository = messageRepository;

        }

        public void SetClientSocket(ISocketWrapper clientSocket, IUserService userService)
        {
            _clientSocket = clientSocket;
            _userService = userService;
        }
        

        public void ReadMessage(string loggedInUser)
        {
            var message = _messageRepository.GetFirstMessageForUser(loggedInUser);

            if (message != null)
            {
                SendData(message);

                _messageRepository.DeleteMessage(message);
            }
            else
            {
                SendData("There are no new messages.");
            }
        }

        
        public void SendMessage()
        {
            SendData("Enter username:");
            string username = ReceiveData();

            if (_userService.UserExists(username))
            {
                SendData("Type your message:");
                string message = ReceiveData();

                int count = _messageRepository.GetMessageCountForUser(username);

                if (count < 5)
                {
                    _messageRepository.StoreMessageInDatabase(username, message);
                    SendData("Message has been sent.");
                }
                else
                {
                    SendData("Mailbox is full.");
                }
            }
            else
            {
                SendData("User doesn't exist.");
            }
        }


        private void SendData(string message)
        {
            string jsonMsg = JsonConvert.SerializeObject(new { command = message });
            byte[] msg = Encoding.ASCII.GetBytes(jsonMsg);
            _clientSocket.Send(msg);
        }
        
        private string ReceiveData()
        {
            byte[] bytes = new byte[1024];
            int numByte = _clientSocket.Receive(bytes);
            string jsonString = Encoding.ASCII.GetString(bytes, 0, numByte);
            dynamic jsonResponse = JsonConvert.DeserializeObject(jsonString);
            return jsonResponse.command;
        }
    }
}