﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Server.Interfaces;

namespace Server.Services;

public class ConnectionPoolService : IConnectionPoolService
{
    private readonly string _connectionString;
    private readonly int _poolSize;
    private readonly int _maxPoolSize;
    private readonly object _lock = new object();
    private readonly List<IDbConnectionWrapper> _availableConnections = new List<IDbConnectionWrapper>();
    private readonly HashSet<IDbConnectionWrapper> _inUseConnections = new HashSet<IDbConnectionWrapper>();

    public ConnectionPoolService(
        string connectionString, 
        int poolSize = 10,
        int maxPoolSize = 100)
    {
        _connectionString = connectionString;
        _poolSize = poolSize;
        _maxPoolSize = maxPoolSize;

        for (int i = 0; i < _poolSize; i++)
        {
            _availableConnections.Add(CreateNewConnection());
        }
    }

    public virtual IDbConnectionWrapper TakeConnection()
    {
        lock (_lock)
        {
            if (_availableConnections.Count == 0 && _inUseConnections.Count >= _maxPoolSize)
            {
                throw new InvalidOperationException("No available connections. Max pool size reached.");
            }

            IDbConnectionWrapper connection = null;

            if (_availableConnections.Count > 0)
            {
                connection = _availableConnections[0];
                _availableConnections.RemoveAt(0);
            }
            else
            {
                connection = CreateNewConnection();
            }

            _inUseConnections.Add(connection);
            return connection;
        }
    }

    public virtual void ReturnConnection(IDbConnectionWrapper connection)
    {
        if (connection == null) return;

        lock (_lock)
        {
            _inUseConnections.Remove(connection);

            if (_availableConnections.Count < _poolSize || !_availableConnections.Contains(connection))
            {
                _availableConnections.Add(connection);
            }
            else
            {
                connection.Dispose();
            }
        }
    }

    private IDbConnectionWrapper CreateNewConnection()
    {
        var connection = new DbConnectionWrapper(_connectionString);
        connection.Open();
        return connection;
    }
}

