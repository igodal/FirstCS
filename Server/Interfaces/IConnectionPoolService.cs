﻿using System.Data.SqlClient;

namespace Server.Interfaces;

public interface IConnectionPoolService
{
    IDbConnectionWrapper TakeConnection();
    void ReturnConnection(IDbConnectionWrapper connection);
}