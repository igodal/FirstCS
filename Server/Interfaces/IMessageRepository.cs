﻿namespace Server.Interfaces;

public interface IMessageRepository
{
    int GetMessageCountForUser(string username);
    void StoreMessageInDatabase(string username, string message);
    string GetConnectionString();
    void DeleteMessage(string message);
    string GetFirstMessageForUser(string username);
}
