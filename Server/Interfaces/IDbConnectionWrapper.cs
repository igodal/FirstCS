﻿using System;
using System.Data.SqlClient;

namespace Server.Interfaces;

public interface IDbConnectionWrapper : IDisposable
{
    void Open();
    void Close();
    SqlCommand CreateCommand();
}
