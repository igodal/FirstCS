﻿using System.Data.SqlClient;
using System.IO;
using System.Text.Json;
using Server.Interfaces;

namespace Server.Repositories;

public class MessageRepository(IConnectionPoolService connectionPoolService) : IMessageRepository
{
    public int GetMessageCountForUser(string username)
    {
        var connection = connectionPoolService.TakeConnection();
        string query = "SELECT COUNT(*) FROM Messages WHERE Username = @Username";
        using (var command = connection.CreateCommand())
        {
            command.CommandText = query;
            command.Parameters.AddWithValue("@Username", username);
            var executedCommand = (int)command.ExecuteScalar();
            connectionPoolService.ReturnConnection(connection);
            return executedCommand;
        }
    }

    public void StoreMessageInDatabase(string username, string message)
    {
        var connection = connectionPoolService.TakeConnection();

        string query = "INSERT INTO Messages (Username, Message) VALUES (@Username, @Message)";
        
        using (var command = connection.CreateCommand())
        {
            command.CommandText = query;
            command.Parameters.AddWithValue("@Username", username);
            command.Parameters.AddWithValue("@Message", message);
            command.ExecuteNonQuery();
            connectionPoolService.ReturnConnection(connection);
        }
        
    }
    public string GetConnectionString()
    {
        string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
        string json = File.ReadAllText(Path.Combine(projectDir, "appsettings.json"));
        
        using (JsonDocument doc = JsonDocument.Parse(json))
        {
            JsonElement root = doc.RootElement;
            JsonElement connectionStrings = root.GetProperty("ConnectionStrings");
            return connectionStrings.GetProperty("FirstCSDbConnection").GetString();
        }    
    }
    
    public void DeleteMessage(string message)
    {
        var connection = connectionPoolService.TakeConnection();

        string query = "DELETE FROM Messages WHERE Message = @Message";
        using (var command = connection.CreateCommand())
        {
            command.CommandText = query;
            command.Parameters.AddWithValue("@Message", message);
            command.ExecuteNonQuery();
            connectionPoolService.ReturnConnection(connection);
        }
        
    }
    
    public string GetFirstMessageForUser(string username)
    {
        var connection = connectionPoolService.TakeConnection();

        string query = "SELECT TOP 1 Message FROM Messages WHERE Username = @Username ORDER BY SentAt ASC";
        using (var command = connection.CreateCommand())
        {
            command.CommandText = query;
            command.Parameters.AddWithValue("@Username", username);
            
            using (SqlDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    return reader.GetString(0);
                }
            }
        }
        connectionPoolService.ReturnConnection(connection);
        return null;
    }
}